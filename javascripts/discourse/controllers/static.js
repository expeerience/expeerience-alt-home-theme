import Controller, { inject as controller } from "@ember/controller";
import { ajax } from "discourse/lib/ajax";
import discourseComputed from "discourse-common/utils/decorators";
import { equal } from "@ember/object/computed";
import { userPath } from "discourse/lib/url";

export default Controller.extend({
  application: controller(),

  showLoginButton: equal("model.path", "login"),

  inti(){
    this.super(...arguments)
    const mainOutlet =  $('#main-outlet')
    mainOutlet.css('background-image', settings.backgroundim_image);
    mainOutlet.addClass("login-pages")
  },

  @discourseComputed("model.path")
  bodyClass: (path) => `static-${path}`,

  @discourseComputed("model.path")
  showSignupButton() {
    return (
      this.get("model.path") === "login" && this.get("application.canSignUp")
    );
  },

  actions: {
    markFaqRead() {
      const currentUser = this.currentUser;
      if (currentUser) {
        ajax(userPath("read-faq"), { type: "POST" }).then(() => {
          currentUser.set("read_faq", true);
        });
      }
    },
  },
});
